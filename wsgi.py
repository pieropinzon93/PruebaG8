#!/usr/bin/python
import os, sys

virtenv = os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/'
virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
try:
    execfile(virtualenv, dict(__file__=virtualenv))
except IOError:
    pass


from pyramid.paster import get_app, setup_logging
ini_path = '/var/lib/openshift/55dbc74e89f5cf0f2800015a/app-root/runtime/repo/production.ini'
setup_logging(ini_path)
application = get_app(ini_path, 'main')


# Below for testing only
if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    httpd = make_server('localhost', 8051, application)
    # Wait for a single request, serve it and quit.
    httpd.handle_request()
